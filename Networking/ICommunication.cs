//-----------------------------------------------------------------------
// <author> 
//     Parth Patel
// </author>
//
// <date> 
//     26-09-2018 
// </date>
// 
// <reviewer> 
//     Libin N George 
// </reviewer>
// 
// <copyright file="ICommunication.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      Interface and other essential types and delegates for Communication Class
// </summary>
//-----------------------------------------------------------------------
namespace Masti.Networking
{
    using System.Net;

    /// <summary>
    /// Prototype of function triggered when data is received
    /// </summary>
    /// <param name="data"> data received (string)</param>
    /// <param name="fromIP">IP address from which data is received</param>
    public delegate void DataReceivalHandler(string data, IPAddress fromIP);

    /// <summary>
    /// Prototype of function triggered to notify the status of data transfer
    /// </summary>
    /// <param name="data">data being sent</param>
    /// <param name="status">status(success or failure)</param>
    public delegate void DataStatusHandler(string data, StatusCode status);

    /// <summary>
    /// Status after sending
    /// </summary>
    public enum StatusCode
    {
        /// <summary>
        /// Send Success 
        /// </summary>
        Success,

        /// <summary>
        /// Send Failure
        /// </summary>
        Failure
    }

    /// <summary>
    /// Module type
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// message module
        /// </summary>
        Message,

        /// <summary>
        /// Image sharing module
        /// </summary>
        ImageSharing
    }

    /// <summary>
    /// communication Interface
    /// </summary>
    public interface ICommunication
    {
        /// <summary>
        /// Sends the data(message) to Target IP
        /// </summary>
        /// <param name="msg">Message or Data to be send</param>
        /// <param name="targetIP">IP to which data has to be send </param>
        /// <param name="type">Data type of the message (Message or ImageSharing)</param>
        /// <returns>returns True if successfully added to the queue otherwise False</returns>
        bool Send(string msg, IPAddress targetIP, DataType type);

        /// <summary>
        /// Subscribes to get notified by event handler in receiving packets specified by given type
        /// </summary>
        /// <param name="type">Data type for which subscription has to be done </param>
        /// <param name="receivalHandler">Event handler to be called on receiving Data</param>
        /// <returns>returns true on successfully subscribing</returns>
        bool SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler);

        /// <summary>
        /// Subscribes to get notified by event handler in sending packets specified by given type
        /// </summary>
        /// <param name="type">Data type of message</param>
        /// <param name="statusHandler">Event handler to be called on successfully sending data</param>
        /// <returns>returns true on successfully subscribing</returns>
        bool SubscribeForDataStatus(DataType type, DataStatusHandler statusHandler);
    }

    /// <summary>
    /// Interface for Singleton Factory that produces ICommunication objects
    /// </summary>
    public interface ICommunicationFactory
    {
        /// <summary>
        /// Returns ICommunicator Instance 
        /// </summary>
        /// <returns>Returns instance of class that implements this interface.</returns>
        ICommunication GetCommunicator();
    }
}
