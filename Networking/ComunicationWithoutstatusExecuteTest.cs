﻿// -----------------------------------------------------------------------
// <author> 
//      Rohith Reddy G
// </author>
//
// <date> 
//      28/10/2018
// </date>
// 
// <reviewer>
//      Libin, Parth, Jude 
// </reviewer>
//
// <copyright file="ComunicationWithoutstatusExecuteTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file contains the test case for a basic functionality without subscribing for data status
//      the data receival module will receive the data 
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// This class executes a basic test in which data stsus is not subscribed
    /// </summary>
    public class ComunicationWithoutstatusExecuteTest : ITest
    {
        /// <summary>
        /// event to pause the thread from executing
        /// </summary>
        private static ManualResetEvent manualReset = new ManualResetEvent(false);

        /// <summary>
        /// schema module instance
        /// </summary>
        private ISchema schema = new MessageSchema();

        /// <summary>
        /// pointer to check the data received
        /// </summary>
        private bool pointer = false;

        /// <summary>
        /// message to be sent 
        /// </summary>
        private string message = "hi";

        /// <summary>
        /// ip address of the machine to which data is sent 
        /// </summary>
        private IPAddress ip = IPAddress.Parse("169.254.25.29");

        /// <summary>
        /// Initializes a new instance of the <see cref="ComunicationWithoutstatusExecuteTest" /> class.
        /// </summary>
        /// <param name="logger">Logger object used for logging during test</param>
        public ComunicationWithoutstatusExecuteTest(ILogger logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Gets or sets Logger instance.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// This function is the part of ITest interface from where the execution starts 
        /// </summary>
        /// <returns>true or false</returns>
        public bool Run()
        {
            /* student object is instantiated */
            using (Communication studentCommunication = new Communication("169.254.25.29", 8000, 3, 1))
            { 
                using (Communication profCommunication = new Communication(8000, 3, 1))
                {
                    /* professor object is instantiated */
                    profCommunication.SubscribeForDataReceival(DataType.Message, this.ProfDataReceival);

                    /* message dict */
                    Dictionary<string, string> messageDict = new Dictionary<string, string>();

                    /* message added to dict */
                    messageDict.Add("message", this.message);

                    /* packet to be sent */
                    string packet = this.schema.Encode(messageDict);

                    /* send function */
                    studentCommunication.Send(packet, this.ip, DataType.Message);

                    manualReset.WaitOne();
                }
            }
            
            if (this.pointer)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This function is called when a message is received
        /// We can extract the ip and data from this function 
        /// </summary>
        /// <param name="data">message data</param>
        /// <param name="fromIP">ip of the sender</param>
        private void ProfDataReceival(string data, IPAddress fromIP)
        {
            IDictionary<string, string> messageDict = this.schema.Decode(data, false);
            if (messageDict["message"] == this.message)
            {
                this.Logger.LogSuccess("successfully got the message");
                this.pointer = true;
            }

            manualReset.Set();
        }
    }
}
