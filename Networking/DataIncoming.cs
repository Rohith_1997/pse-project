﻿// -----------------------------------------------------------------------
// <author> 
//      Rajat Sharma (rajat4803@gmail.com)
// </author>
//
// <date> 
//      19-10-2018 
// </date>
// 
// <reviewer>
//      Ayush Mittal
//      Libin N George
// </reviewer>
//
// <copyright file="DataIncoming.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file contains Data Receiver 
//      Receive Messages from Clients and send it to DataReceiverNotifier
//      Functions for connecting clients and handling them
//      This file is a part of Networking Module
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using Masti.QualityAssurance;

    /// <summary>
    /// Data Receiver which accepts connections from clients
    /// Receives Message from Clients and pass it to DataRecevialNotifier
    /// </summary>
    public partial class Communication
    {
        /// <summary>
        /// 4 ReservedBits for Message Length and 1 bit to identify actual message vs acknowledge message 
        /// </summary>
        private int reservedBits = 5;

        /// <summary>
        /// Start the Receiver by creating a socket listening for incoming connections
        /// </summary>
        /// <returns>Thread which waits for client connection</returns>
        public Thread StartReceiver()
        {
            if (this.isStudent)
            {
                // This part of code shouldn't be executed on student's machine
                throw new InvalidProgramException();
            }

            // Creating and Starting a TcpListener to listen for incoming client connections 
            // if testIP is empty then instantiating normal listener
            // if testIP is not empty then instantiating test listener
            if (string.IsNullOrEmpty(this.testIP))
            {
                IPHostEntry ipHostEntry = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress[] ip = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
                this.ipAddress = ip[0].ToString();
                this.serverSocket = new TcpListener(ip[0], this.port);
            }
            else
            {
                this.serverSocket = new TcpListener(IPAddress.Parse(this.testIP), this.port);
            }

            try
            {
                this.serverSocket.Start();
                MastiDiagnostics.LogInfo($"DataIncoming: Server Started Successfully and Listening on Port {this.port} ");
            }
            catch
            {
                MastiDiagnostics.LogInfo($"DataIncoming: Failed to Start Server on Port {this.port} ");
                throw;
            }

            return new Thread(this.WaitForClients);
        }

        /// <summary>
        /// Waiting to connect with Clients
        /// Begins an asynchronous operation to accept an incoming connection
        /// </summary>
        private void WaitForClients()
        {
            // BeginAcceptTcpClient takes two parameters 1) callback: AsyncCallback, 2) state: Object
            // An AsyncCallback delegate that points to the method that should be called when the operation completes.
            // A custom object that contains information about the acceptance process. This object is passed to the delegate when the operation completes. 
            // Returns IAsyncResult
            try
            {
                this.serverSocket.BeginAcceptTcpClient(new AsyncCallback(this.OnClientConnected), null);
            }
            catch
            {
                MastiDiagnostics.LogError($"DataIncoming: Problem with BeginAcceptTcpClient");
                this.isRunning = false;
            }

        }

        /// <summary>
        /// Accepting Connection from Client and calling Handle request for Client
        /// </summary>
        /// <param name="asyncResult">An IAsyncResult that points to the asynchronous creation of the TcpClient.</param>
        private void OnClientConnected(IAsyncResult asyncResult)
        {
            // Asynchronously accepts an incoming connection attempt and creates a new TcpClient to handle remote host communication. 
            TcpClient tcpClient = null;
            try
            {
                tcpClient = this.serverSocket.EndAcceptTcpClient(asyncResult);
                this.serverSocket.BeginAcceptTcpClient(new AsyncCallback(this.OnClientConnected), null);
            }
            catch
            {
                MastiDiagnostics.LogError($"DataIncoming: Problem with BeginAcceptTcpClient");
                this.isRunning = false;
            }
           
            if (tcpClient != null)
            {
                string incomingClientIpAddress = Helper.GetEndPointAddress(tcpClient);
                string incomingClientPort = Helper.GetEndPoint(tcpClient);

                // Acquiring lock and filling data in hash tables which is to be used by DataOutGoing Module 
                lock (this.mainLock)
                {
                    if (!this.lockObjects.ContainsKey(incomingClientPort))
                    {
                        this.lockObjects[incomingClientPort] = new object();
                    }

                    if (!this.lockStatus.ContainsKey(incomingClientPort))
                    {
                        this.lockStatus[incomingClientPort] = false;
                    }

                    MastiDiagnostics.LogInfo($"DataIncoming: Client with IP : {incomingClientIpAddress} is added to Connected Clients hash-table");
                    this.connectedClients[incomingClientIpAddress] = tcpClient.Client;
                }

                MastiDiagnostics.LogInfo($"DataIncoming: Client with IP : {incomingClientIpAddress} Connected");

                // Call to a function which handles receiving of messages 
                this.HandleClientRequest(tcpClient);
            }
        }

        /// <summary>
        /// Handles the Clients by Receiving Message from Client and Passing it to DataReceiveNotifier
        /// In case of Acknowledgement Message it notifies the DataOutGoing Module
        /// </summary>
        /// <param name="tcpClient">Client Socket</param>
        private void HandleClientRequest(TcpClient tcpClient)
        {
            // Getting network stream to read messages 
            NetworkStream networkStream = tcpClient.GetStream();

            // Number of Bytes Read from the Network Stream in one go 
            int numberOfBytesRead;

            // Length of Whole Message which is identified from first 4 reserved bits 
            int messageLength = 0;

            // To distinguish actual messages from acknowledged messages 
            int acknowledgement = -1;

            // Offset for message within the packet 
            int offset = 0;
            int numberOfReservedBitsOfMessageInPacket = 0;
            int loopIndex = 0;
            StringBuilder tempBufferForMessage = new StringBuilder();

            // Helper Variable to find length of message from reserved bits
            int seedNum = 0;

            // Used to keep track of number of reserved bytes read
            int numberOfReservedBytesRead = 0;

            MastiDiagnostics.LogInfo($"DataIncoming: Started to Receive Message From Client : {Helper.GetEndPointAddress(tcpClient)} ");
            while (tcpClient.Connected)
            {
                byte[] buffer = new byte[tcpClient.ReceiveBufferSize];
                try
                {
                    numberOfBytesRead = networkStream.Read(buffer, 0, tcpClient.ReceiveBufferSize);
                    loopIndex = 0;
                    numberOfReservedBitsOfMessageInPacket = 0;

                    // Finding the length of Message from reserved bits 
                    if (numberOfReservedBytesRead < this.reservedBits - 1)
                    {
                        int temp = 0;
                        for (loopIndex = 0; loopIndex < Math.Min(this.reservedBits - 1 - numberOfReservedBytesRead, numberOfBytesRead - numberOfReservedBytesRead); loopIndex++)
                        {
                            temp = (temp << 8) | buffer[loopIndex];
                        }

                        seedNum = (seedNum << (loopIndex * numberOfReservedBytesRead)) | temp;
                        numberOfReservedBytesRead += loopIndex;
                        numberOfReservedBitsOfMessageInPacket += loopIndex;
                        if (numberOfReservedBytesRead >= this.reservedBits - 1)
                        {
                            messageLength = seedNum;
                            MastiDiagnostics.LogInfo($"DataIncoming: Length of Received Message From Client {Helper.GetEndPointAddress(tcpClient)} is {messageLength}");
                        }
                        else
                        {
                            continue;
                        }
                    }

                    // Extracting Acknowledgement Bit from the Message 
                    if (numberOfReservedBytesRead < this.reservedBits)
                    {
                        acknowledgement = buffer[loopIndex];
                        numberOfReservedBytesRead += 1;
                        numberOfReservedBitsOfMessageInPacket += 1;
                    }

                    // If it the last packet of the message then based on acknowledgement bit notifying Modules
                    while (messageLength <= numberOfBytesRead - numberOfReservedBitsOfMessageInPacket && numberOfBytesRead > numberOfReservedBitsOfMessageInPacket)
                    {
                        tempBufferForMessage.Append(Encoding.ASCII.GetString(buffer, offset + numberOfReservedBitsOfMessageInPacket, messageLength));
                        string finalMessage = tempBufferForMessage.ToString();
                        MastiDiagnostics.LogInfo($"DataIncoming: Final Message Received from Client {Helper.GetEndPointAddress(tcpClient)} is {finalMessage}");

                        // Acknowledge 1 means Acknowledge Message and Acknowledgement 0 means Actual Message
                        if (acknowledgement == 1)
                        {
                            this.Acknowledge(finalMessage + Helper.GetEndPoint(tcpClient));
                            tempBufferForMessage.Clear();
                            MastiDiagnostics.LogSuccess($"DataIncoming: Acknowledgement Message Successfully Received from Client {Helper.GetEndPointAddress(tcpClient)}");
                        }
                        else if (acknowledgement == 0)
                        {
                            IPAddress ipAddress = IPAddress.Parse(((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString());
                            this.SendHelper(Helper.MD5Hash(finalMessage), 0, tcpClient.Client, 1);
                            this.DataReceiveNotifier(finalMessage, ipAddress);
                            tempBufferForMessage.Clear();
                            MastiDiagnostics.LogSuccess($"DataIncoming: Actual Message Successfully Received from Client {Helper.GetEndPointAddress(tcpClient)}");
                        }

                        // Extracting numberofBytes read of next message 
                        numberOfBytesRead -= messageLength + numberOfReservedBitsOfMessageInPacket;

                        // finding acknowledgement bit, length and numberofReservedBytes of next Message
                        if (numberOfBytesRead > this.reservedBits - 1)
                        {
                            MastiDiagnostics.LogInfo($"DataIncoming: Packet had more than one message");
                            acknowledgement = buffer[messageLength + offset + numberOfReservedBitsOfMessageInPacket + this.reservedBits - 1];
                            byte[] tempBufferForCalculatingLength = new byte[this.reservedBits - 1];

                            // Finding the length in decimal format from given reserved bits in the message
                            Array.Copy(buffer, messageLength + offset + numberOfReservedBitsOfMessageInPacket, tempBufferForCalculatingLength, 0, this.reservedBits - 1);
                            if (BitConverter.IsLittleEndian)
                            {
                                Array.Reverse(tempBufferForCalculatingLength);
                            }

                            int lengthOfNewMessage = BitConverter.ToInt32(tempBufferForCalculatingLength, 0);
                            offset += messageLength + numberOfReservedBitsOfMessageInPacket;
                            messageLength = lengthOfNewMessage;
                            MastiDiagnostics.LogInfo($"DataIncoming: Length of Received Message From Client {Helper.GetEndPointAddress(tcpClient)} is {messageLength}");
                            numberOfReservedBytesRead = this.reservedBits;
                            numberOfReservedBitsOfMessageInPacket = this.reservedBits;
                        }
                        else
                        {
                            MastiDiagnostics.LogInfo($"DataIncoming: Packet had more than one message");
                            messageLength = 0;
                            seedNum = 0;
                            numberOfReservedBytesRead = numberOfBytesRead;
                            for (int j = 0; j < numberOfBytesRead; j++)
                            {
                                seedNum = (seedNum << 8) | buffer[j];
                            }

                            numberOfReservedBitsOfMessageInPacket = 0;
                            offset = 0;
                        }
                    }

                    // If it is the first packet of message and length is greater than number of bytes read minus reserved bits then storing message in temp buffer 
                    // and decrementing length by number of bytes read minus reserved bits and setting isFirst for next packets to be false 
                    // If it is not the first packet and length is greater than number of bytes read then storing message in temp buffer 
                    // and decrementing length by number of bytes read 
                    if (messageLength >= (numberOfBytesRead - numberOfReservedBitsOfMessageInPacket) && numberOfBytesRead > numberOfReservedBitsOfMessageInPacket)
                    {
                        tempBufferForMessage.Append(Encoding.ASCII.GetString(buffer, offset + numberOfReservedBitsOfMessageInPacket, numberOfBytesRead - numberOfReservedBitsOfMessageInPacket));
                        messageLength -= numberOfBytesRead - numberOfReservedBitsOfMessageInPacket;
                    }

                    offset = 0;
                }
                catch
                {
                    MastiDiagnostics.LogInfo($"DataIncoming: Client Disconnected");
                }
            }
        }
    }
}