# Diagnostics
_Prepared by Adrian McDonald Tariang, 111501001 on 12-09-2018_

## Implementation

### Details
- Allow the module developers to track execution of their components in develpment and production builds, in order to track bugs and errors
- Use the File Logger from the Test Harness for this feature
- Use a Singleton Design Pattern so as to have a Single Logger Object shared by each of the modules
- All the logging through a program execution is stored in a single log file
- Log naming pattern followed would be Log[date][lognumber].txt
- The developer can change verbosity of logging in the development build
- Provide an email feature to send the diagnostics data to the developers when an exception or program failure occurs

### Interface 
The Developer will be using an instance of a Logger to write to their diagnostics
```csharp
	public interface ILogger
    {
        void LogInfo(string message);
        void LogWarning(string message);
        void LogError(string message);
        void LogSuccess(string message);

        // Send the logs to a prespecified mail id.
        void MailLog();
    }
```

### Usage
The Developers can use the Diagnostics just as the Logger in their Test implementations
```csharp
// An Example Module feature
public class FruitStore 
{
	public boolean SellFruit(string fruitName, int soldQuantity) 
	{
		Diagnostics.LogInfo($"Start a transaction for {fruitName}");
		...

		if (success) {
			Diagnostics.LogSuccess("Transaction Success");
		} else {
			Diagnostics.LogError("Transaction Failed");
		}
	}
}
```

When a Run-Time Error is caught, an email with the log can be sent to the developers for analysis. The email address will be predefined in the application build
```csharp
	try {
		...
	} catch(Exception e) {
		Diagnostics.LogError($"Error: {e.Message}");
		...

    	Diagnostics.MailLog();
	}
```

### Example Output
Output for the example code from `FruitStore.SellFruit` function in a log file named `Log09_13_2018_001.txt`

**[Production Build]**
```
Start a transaction for Apple
Transaction Success
```

**[Development Build]**
```
[FruitStore.SellFruit] Start a transaction for Apple
[FruitStore.SellFruit] Transaction Success
```

### Notes for Development
- Email can be send using the `SmtpClient` feature provided by the .NET Framework
- The Logger is available to all module through the use of the Singleton Design Pattern
- The log is held in a `StringBuilder` object throughout the execution of the program and is only written to a file, during the termination sequence
- The `ClassName.FunctionName` annotations can be extracted through the `System.Diagnostics.StackTrace` Class provided by the .NET framework
- Unlike the TestHarness where the developer specifies the verbosity level in the commandline parameter, the Diagnostics can set the level within the `app.config` file 